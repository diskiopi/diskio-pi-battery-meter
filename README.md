diskio-pi-battery-meter
=======================

Install on Raspberry Pi
-----------------------

```
sudo apt install raspberrypi-kernel-headers device-tree-compiler dkms git
```

Installation for 1.0A board
```
git clone https://gitlab.com/diskiopi/diskio-pi-battery-meter.git
cd ~/diskio-pi-battery-meter/deb
sudo dpkg -i dpi-battery-meter_1.0.x_armhf.deb
```

Installation for 1.0B & 1.2 board
```
git clone https://gitlab.com/diskiopi/diskio-pi-battery-meter.git
cd ~/diskio-pi-battery-meter/deb
sudo dpkg -i dpi-battery-meter_1.2.x_armhf.deb
```


Compile on Raspberry Pi
-----------------------

First, install the development packages:

```
sudo apt install raspberrypi-kernel-headers device-tree-compiler
```

Add a module to load in /etc/modules
```
i2c-dev
i2c-bcm2708
```
Add a module to load in /boot/config.txt
```
dtparam=i2c_arm=on
dtoverlay=i2c-rtc,ds1307
```

Reboot :
```
sudo reboot
```

Clone the driver :
```
git clone https://gitlab.com/diskiopi/diskio-pi-battery-meter.git
cd diskio-pi-battery-meter
```

Edit the max1726x_battery.h file and set the DISKIO_PI_BOARD_1_0 variable (see below):

For 1.0A boards, line 35:
```
#define DISKIO_PI_BOARD_1_0 1
```
For 1.0B and 1.2 boards, line 35 (set by default):
```
#define DISKIO_PI_BOARD_1_0 0
```

Build the driver:
```
make
```

Install the driver:

```
sudo insmod max1726x_battery.ko
```

Build and install the overlay:

```
dtc -O dtb -o max1726x_battery.dtbo -b 0 -@ max1726x_battery.dts
sudo dtoverlay max1726x_battery.dtbo
```

Then check if the applet indicates the same thing as the analogue gauge.

After kernel update, you must recompile the driver.


How to dertermine the board version
-----------------------------------

Version 1.0A: squared header
![an example of the applet](1.0A_board.jpg)

Version 1.0B or 1.2: header with connector
![an example of the applet](1.0B_1.2_board.jpg)

Screenshot
----------

![an example of the applet](indicateur_batt.jpg)


