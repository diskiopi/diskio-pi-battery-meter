// MAX17263 test
// Author : Mace Robotics
// Project : Diskio Pi
// Doc : MAX1726x Software Implementation Guide
//
#include <Wire.h>

#define ADR_MAX17263 0x36
#define MAX17263_VCELL 0x09
#define MAX17263_SOC 0x06
void setup()
{
  // A4 (SDA), A5 (SCL)
  Serial.begin(115200);  // start serial for output
}

void loop()
{
float tension;
float Percentage;
  //Serial.print("Status Register:");
  //Serial.println(read_MAX17263(0x21), HEX);

  Serial.print("Battery voltage:");
  tension = (float)(read_MAX17263(MAX17263_VCELL));
  tension = tension * 0.000078125 * 2;// x2 => 2 CELLs
  Serial.println(tension);

  Serial.print("Battery Percentage:");
  Percentage = (float)(read_MAX17263(MAX17263_SOC)*0.003906f); // 1/256 %
  Serial.println(Percentage);
  
  delay(1000);           
}


uint16_t read_MAX17263(uint8_t reg)
{
uint16_t temp, temp1;

  Wire.begin();
  Wire.beginTransmission(ADR_MAX17263);
  Wire.write(reg);
  Wire.endTransmission();
  Wire.requestFrom(ADR_MAX17263, 2);
  temp = (uint16_t)Wire.read() ;
  temp1 = (uint16_t)Wire.read() << 8;
  temp = temp + temp1;
  Wire.endTransmission();
  
  return temp;
}

// End of file
